profile-sync-daemon (6.50-1) unstable; urgency=medium

  * QA upload.
  * New upstream release.
  * debian/patches/: Dropped, merged upstream.

 -- Boyuan Yang <byang@debian.org>  Mon, 20 Nov 2023 22:07:30 -0500

profile-sync-daemon (6.49-1) unstable; urgency=medium

  * QA upload.
  * Bump Standards-Version to 4.6.2, debhelper compat v13.
  * debian/control: Set maintainer to Debian QA Group.
  * New upstream version 6.49. (Closes: #1026265)
  * debian/watch: Update to v4 format.
  * debian/control: Update Vcs-* fields.
  * debian/patches: Dropped old patches, merged upstream.
  * debian/patches/0001-Fix-man-section-number-in-doc-psd.1.patch:
    Fix man page section to circumvent build error by dh_installman.

 -- Boyuan Yang <byang@debian.org>  Sun, 01 Oct 2023 11:05:43 -0400

profile-sync-daemon (6.34-1.2) unstable; urgency=medium

  * Non-maintainer upload.
  * Add /sbin to PATH. (Closes: #932345)

 -- Shengjing Zhu <zhsj@debian.org>  Tue, 29 Nov 2022 23:45:53 +0800

profile-sync-daemon (6.34-1.1) unstable; urgency=medium

  * Non-maintainer upload.
  * No source change upload to rebuild with debhelper 13.10.

 -- Michael Biebl <biebl@debian.org>  Sat, 15 Oct 2022 11:05:13 +0200

profile-sync-daemon (6.34-1) unstable; urgency=medium

  * New upstream release.
  * debian/compat: Update to version 12.
  * debian/control:
    - Update to Debian policy 4.3.0, no changes needed.

 -- Jan Luca Naumann <j.naumann@fu-berlin.de>  Sat, 02 Mar 2019 14:48:44 +0100

profile-sync-daemon (6.31-1) unstable; urgency=medium

  * New upstream release.

 -- Jan Luca Naumann <j.naumann@fu-berlin.de>  Wed, 21 Dec 2016 17:33:48 +0100

profile-sync-daemon (6.28-1) unstable; urgency=medium

  * New upstream release.

 -- Jan Luca Naumann <j.naumann@fu-berlin.de>  Sun, 09 Oct 2016 22:06:25 +0200

profile-sync-daemon (6.26-1) unstable; urgency=medium

  * New upstream release.
    - debian/patches: Remove fix-bash-4.4-substitution.patch, merged upstream.

 -- Jan Luca Naumann <j.naumann@fu-berlin.de>  Sun, 02 Oct 2016 21:55:49 +0200

profile-sync-daemon (6.25-2) UNRELEASED; urgency=medium

  * debian/patches:
    - Add fix-bash-4.4-substitution.patch to fix
      string substitution issue with bash 4.4. (Closes: #839049)

 -- Jan Luca Naumann <j.naumann@fu-berlin.de>  Thu, 29 Sep 2016 17:19:11 +0200

profile-sync-daemon (6.25-1) unstable; urgency=low

  * New upstream release.

 -- Jan Luca Naumann <j.naumann@fu-berlin.de>  Sat, 09 Jul 2016 17:10:08 +0200

profile-sync-daemon (6.23-1) unstable; urgency=low

  * New upstream release.
  * Use upstream-provided manpage doc/psd-overlay-helper.1.

 -- Jan Luca Naumann <j.naumann@fu-berlin.de>  Tue, 14 Jun 2016 22:18:22 +0200

profile-sync-daemon (6.22-1) UNRELEASED; urgency=low

  * Initial release. (Closes: #827127)

 -- Jan Luca Naumann <j.naumann@fu-berlin.de>  Sun, 12 Jun 2016 18:25:15 +0200
